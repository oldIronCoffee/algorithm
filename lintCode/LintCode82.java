/**
 * Author:   oldIron
 * Date:     2020/11/12 17:06
 * Version: 1.0
 * Description: 落单的数
 */
package lintCode;

import java.util.*;

/**
 * ClassName: LintCode82
 * Author:   oldIron
 * Date:     2020/11/12 17:06
 * Version: 1.0
 * Description: 落单的数
 */
public class LintCode82 {

    /**
     * 落单的数
     * 给出 2 * n + 1个数字，除其中一个数字之外其他每个数字均出现两次，找到这个数字。
     * 举例
     * 输入：[1,1,2,2,3,4,4]
     * 输出：3
     * 解释：仅3出现一次，简单点理解就是找出数组中的唯一数
     */


    /**
     * 本方法是循环数组，放入map中，然后把重复的元素的key 的value + 1，然后遍历map，找到key对应value = 1的那个 key就是了
     * 时间复杂度：O(n)
     * 空间复杂度：O(n)
     */
    public static int singleNumber1(int[] A) {
        int a = 0;
        Map<Integer,Integer> map = new HashMap<>();
        for(int i=0;i<A.length;i++){
            if(map.containsKey(A[i])) {
                map.put(A[i], map.get(A[i]).intValue()+1);
            }else {
                map.put(A[i], new Integer(1));
            }

        }
        Iterator<Integer> iter = map.keySet().iterator();
        while(iter.hasNext()) {
            Integer key = iter.next();
            if(map.get(key) == 1){
                a = key;
            }
        }
        return a;
    }


    /**
     * 本方法是对数组进行排序，如果长度=1就直接返回，然后相邻的2个元素进行比较，如果中间数和前一位比不相等，同时和后一位比也不相等，那么这个数就肯定是我们要找的唯一数。
     * 时间复杂度：O(n)
     * 空间复杂度：O(n)
     */
    public static int singleNumber2(int[] A) {
        int b = 0;
        Arrays.sort(A);
        ArrayList<Integer> arr = new ArrayList<Integer>();
        int n = A.length;
        //如果只有1位数直接返回
        if(n == 1){
            return A[0];
        }
        for(int i =0; i < n; i ++){
            if(i == n-1 && A[i] != A[i - 1]){
                //当i = 最后一位，如果最后一位和倒数第二位不相等，就返回最后一位
                b = A[i];
            }else if(i == 0 && A[i] != A[i + 1]){
                //当i = 第一位，如果第一位和第二位不相等，就返回第一位
                b = A[i];
                break;
            }else{
                //当i = 中间位数，如果这个数 不等于前面的数，同时也不等于后面的数，就返回这个数
                if(i != 0 && i != n-1 && A[i] != A[i - 1] && A[i] != A[i + 1]){
                    b = A[i];
                }
            }
        }
        return b;
    }


    /**
     * 本方法是用利用异或运算的交换规则，相同的异或为0，所有相同的元素异或结果都是0，那么最终的异或结果不为0，就是唯一数
     * 时间复杂度：O(n)
     * 空间复杂度：O(n)
     */
    public static int singleNumber3(int[] A) {
        int b = A[0];
        for(int i = 1; i < A.length; i++){
            b = b ^ A[i];
        }
        return b;
    }

    public static void main(String[] args) {
        int a [] = {1,1,2,2,3,4,4,5,6,5,7,6,7,8,8,9,9,10,10,11,11,12,12,13,13,14,14,15,15,16,16,17,17,18,18,19,19,20,20,21,21,22,22,23,23,24,24,25,25};
        //int b = StringUtil.singleNumber3(a);

        long t1 = System.currentTimeMillis();
        int b1 = LintCode82.singleNumber1(a);
        long t2 = System.currentTimeMillis();
        System.out.println("-----方法1耗时（ms）-----" + (t2 - t1) + "----结果---" + b1);

        long t3 = System.currentTimeMillis();
        int b2 = LintCode82.singleNumber2(a);
        long t4 = System.currentTimeMillis();
        System.out.println("-----方法2耗时（ms）-----" + (t4 - t3) + "----结果---" + b2);

        long t5 = System.currentTimeMillis();
        int b3 = LintCode82.singleNumber3(a);
        long t6 = System.currentTimeMillis();
        System.out.println("-----方法3耗时（ms）-----" + (t6 - t5) + "----结果---" + b3);

    }

}
